//[SECTION] Dependencies and Modules+

	const User = require("../models/User");
	const bcrypt = require("bcrypt");
	const dotenv = require("dotenv");
	const auth = require("../auth")

//[SECTION] Environment Setup

	dotenv.config()
	const salt = parseInt(process.env.SALT);

//[SECTION] Functionalities [CREATE]

	//[SUB-SECTION] [REGISTRATION]

		module.exports.registerUser = (data) => {

			let email = data.email;
			let firstName = data.firstName;
			let lastName = data.lastName;
			let password = data.password;
			let mobileNumber = data.mobileNumber;

			let newUser = new User({
				email: email,
				firstName: firstName,
				lastName: lastName,
				password: bcrypt.hashSync(password, salt),
				mobileNumber: mobileNumber
			});

			return User.findOne({email: data.email}).then(foundExistingUser => {
				if (foundExistingUser){
					return ({message: `Email already registered`})
				} else {

					return newUser.save().then((userCreated, err) => {

						if(userCreated) {
							return userCreated
						} else {
							return ({message: `Failed to Register New Account`});
						};
					});
				}
			})

		};

		//[SUB-SECTION] [AUTHENTICATION]

		module.exports.loginUser = (userData) => {

			let email = userData.email;
			let password = userData.password;

			return User.findOne({email: email}).then(result => {

				if (result === null) {
					return ({message: `${email} is not a Register User`})
				} else {
					return ({message:`Email Found`})
				};
			});
		};

	//[SUB-SECTION] [LOGIN/JWT]

		module.exports.loginUser = (req, res) => {

			User.findOne({email: req.body.email}).then(foundUser => {
				if(foundUser === null){
					return res.send({message: `${req.body.email} Not Found`})
				} else {
					const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

					if(isPasswordCorrect) {

						return res.send({accessToken: auth.createAccessToken(foundUser)})
					} else {
						return res.send({message: "Incorrect Password"})
					}
				}
			}).catch(err => res.send(err))
		};

//[SECTION] Functionalities [RETRIEVE]

	//[SUB-SECTION] [GET MY DETAILS]

		module.exports.getMyDetails = (req, res) => {

			User.findById(req.user.id).then(result => res.send(result)).catch(err => res.send(err));
		};

	//[SUB-SECTION] [GET ALL ADMIN ASSISTANT]

		module.exports.getAllAdminAssistant = () => {

			return User.find({isAdminAssistant: true}).then(resultOfTheQuery => {
				return resultOfTheQuery;
			});
		}

	//[SUB-SECTION] [GET ALL COSTUMER]

		module.exports.getAllCostumer = () => {
			
			return User.find({isAdmin: false, isAdminAssistant: false}).then(resultOfTheQuery => {
				return resultOfTheQuery;
			});
		}

	//[SUB-SECTION] [CALLOUT]

		//[SUB-SECTION] [GET SINGLE PRODUCT]

		module.exports.getUserData = (id) => {
			
			return User.findById(id).then(resultOfQuery => {
				return resultOfQuery;
			})
		};

//[SECTION] Functionalities [UPDATE]

	//[SUB-SECTION] [ASSIGN AN ADMIN ASSISTANT]

		module.exports.hireAssistant = async (userId, result) => {
			
			let updates = {
				isAdminAssistant: true
			}

			let hire = await User.findByIdAndUpdate(userId, updates).then((hire, err) => {
				if (hire) {
					return `You just hired a new Admin Assistant`
				} else {
					return `Failed to hire new Admin Assistant`
				}
			})

			let isLogUpdated = await User.findById(result.id).then(user => {

				let userLog = {

					targetId: userId,
					action: `Hire new Admin Assistant`
				
				}

				user.log.push(userLog);
				return user.save().then(user => true).catch(err => err.message)

			})
		}

	//[SUB-SECTION] [REMOVE AN ADMIN ASSISTANT]

		module.exports.fireAssistant = async (userId, result) => {

			let updates = {
				isAdminAssistant: false
			}

			let fire = await User.findByIdAndUpdate(userId, updates).then((fire, err) => {
				if (fire) {
					return `You just fired an Admin Assistant`
				} else {
					return `Failed to fire an Admin Assistant`
				}
			})

			let isLogUpdated = await User.findById(result.id).then(user => {

				let userLog = {

					targetId: userId,
					action: `Fired an Admin Assistant`
				
				}

				user.log.push(userLog);
				return user.save().then(user => true).catch(err => err.message)

			})
		}

	//[SUB-SECTION] [BAN USER]

		module.exports.banUser = async (userId, result) => {

			let updates = {
				isBan: true
			}

				let ban = await User.findByIdAndUpdate(userId, updates).then((ban, err) => {
					if (ban) {
						return `Account ${userId} will no longer can use this account`
					} else {
						return `Failed to ban the user`
					}
				})

				let isLogUpdated = await User.findById(result.id).then(user => {

				let userLog = {

					targetId: userId,
					action: `Banned a User`
				
				}

				user.log.push(userLog);
				return user.save().then(user => true).catch(err => err.message)

			})
			}

		//}

	//[SUB-SECTION] [UNBAN USER]

		module.exports.unbanUser = async (userId, result) => {

			let updates = {
				isBan: false
			}

			let unban = await User.findByIdAndUpdate(userId, updates).then((fire, err) => {
				if (fire) {
					return `Account can now use this account`
				} else {
					return `Failed to unban the user`
				}
			})

			let isLogUpdated = await User.findById(result.id).then(user => {

				let userLog = {

					targetId: userId,
					action: `Unbanned a User`
				
				}

				user.log.push(userLog);
				return user.save().then(user => true).catch(err => err.message)

			})
		}

		//[SUB-SECTION] [UPDATE MY DETAILS]

		module.exports.updateMyDetails = (id, data) => {

			let email = data.email;
			let firstName = data.firstName;
			let lastName = data.lastName;
			//let password = data.password;
			let mobileNumber = data.mobileNumber;
			
			let updatedUser = {
				email: email,
				firstName: firstName,
				lastName: lastName,
				//password: bcrypt.hashSync(password, salt),
				mobileNumber: mobileNumber
			};

			return User.findByIdAndUpdate(id, updatedUser).then((userUpdated, err) => {

				if(err) {
					return `Failed to Update your profile`
				} else {
					return `Profile Updated`
				};
			});

		};

//[SECTION] Functionalities [DELETE]