//[SECTION] Dependencies and Modules

	const Order = require('../models/Order');
	const Cart = require('../models/Cart');
	const Product = require('../models/Product');

//[SECTION] Functionalities [CREATE]

	module.exports.checkout = async (id, userResult, cartResult, productResult) => {

		let userId = userResult.id
		let userFullName = `${userResult.firstName} ${userResult.lastName}`
		let finalAmount = 0

		let newOrder = new Order ({
			userId: id,
			userFullName: userFullName
			 
		});

		let addOrder = newOrder.save().then((addedOrder, err) => {
			if (err) {
				return `Failed to Order`
			} else {
				
				for (var i = cartResult.length - 1; i >= 0; i--) {
						let c = cartResult[i].quantity;
						let idProduct = cartResult[i].productId;

					Product.findById(idProduct).then(resultOfTheQuery => {
						
						let isOrderUpdated = Order.findById(addedOrder.id).then(order => {						

							let newOrder = {

								productId: resultOfTheQuery.id,
								productName: resultOfTheQuery.name,
								productQuantity: c,
								productPrice: resultOfTheQuery.price,
								totalPrice: c * resultOfTheQuery.price

							}
							
							order.cart.push(newOrder);
							order.save().then(user => true).catch(err => err.message)

							finalAmount = finalAmount + (c * resultOfTheQuery.price)

							let updates = {
								finalPrice: finalAmount
							}
							
							Order.findByIdAndUpdate(addedOrder.id, updates).then(result => {

							})
							
						})

					});

				}
				
			}



		});

		for (var i = cartResult.length - 1; i >= 0; i--) {
					Cart.findByIdAndRemove(cartResult[i].id).then(result => {
						
					})
				}

		

	};

//[SECTION] Functionalities [RETRIEVE]

	//[SUB-SECTION] [GET-ALL ORDERS]

		module.exports.getAllOrders = () => {

			return Order.find({}).then(result => {
				return result;
			});
		};

	//[SUB-SECTION] [GET ALL ACTIVE PRODUCTS]

		module.exports.getMyOrders = (id) => {
			
			return Order.find({userId: id}).then(resultOfTheQuery => {
				return resultOfTheQuery;
			});
		};