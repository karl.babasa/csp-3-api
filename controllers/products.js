//[SECTION] Dependencies and Modules

	const Product = require('../models/Product');
	const User = require('../models/User');

//[SECTION] Functionalities [CREATE]

	module.exports.createProduct = async (info, data,id, result, req, res) => {

		let productName = info.name
		let artistName = info.artist
		let productDescription = info.description
		let productPrice = info.price
		let productImg = info.img
		let newProduct = new Product ({
			name: productName,
			artist: artistName,
			description: productDescription,
			price: productPrice,
			img: productImg
		});
		let addProduct = await newProduct.save().then((savedProduct, err) => {
				if (err) {
					return 'Failed to Add New Product'
				} else {
					
					let isLogUpdated = User.findById(id).then(user => {

					let userLog = {

						targetId: savedProduct.id,
						action: `Created a Product: ${info.name}`
					
					}

					user.log.push(userLog);
					return user.save().then(user => true).catch(err => err.message)

				})
			}
	 	});
	};

//[SECTION] Functionalities [RETRIEVE]
	
	//[SUB-SECTION] [GET-ALL PRODUCTS]

		module.exports.getAllProducts = () => {

			return Product.find({}).then(result => {
				return result;
			});
		};

	//[SUB-SECTION] [GET SINGLE PRODUCT]

		module.exports.getProduct = (id) => {
			
			return Product.findById(id).then(resultOfQuery => {
				return resultOfQuery;
			})
		};

	//[SUB-SECTION] [GET ALL ACTIVE PRODUCTS]

		module.exports.getAllActiveProducts = () => {
			
			return Product.find({isAvailable: true}).then(resultOfTheQuery => {
				return resultOfTheQuery;
			});
		};

//[SECTION] Functionalities [UPDATE]

	//[SUB-SECTION] [UPDATE PRODUCT]

		module.exports.updateProduct = async (id, details, result, req, res) => {
		
			let productName = details.name;
			let productDescription = details.description;
			let productPrice = details.price;
			let productArtist = details.artist;
			let productImg = details.img;
			
			let updatedProduct = {
				name: productName,
				description: productDescription,
				price: productPrice,
				artist: productArtist,
				img: productImg
			}
			
			let updateProduct = await Product.findByIdAndUpdate(id, updatedProduct).then((productUpdated, err) => {
				
				if (err) {
					return 'Failed to update Product';
				} else {
					return 'Successfully Updated Product';
				}
			})

			let isLogUpdated = await User.findById(result.id).then(user => {

				let userLog = {

					targetId: id,
					action: `Updated a Product : Name to ${productName}, Description to ${productDescription}, Price to ${productPrice}`
				
				}

				user.log.push(userLog);
				return user.save().then(user => true).catch(err => err.message)

			})
		}

		//[SUB-SECTION] [DEACTIVATE PRODUCT] [SOFT DELETE]

		module.exports.deactivateProduct = async (id, result, req, res) => {
			
			let updates = {
				isAvailable: false
			}
			
			let productdeactivate = await Product.findByIdAndUpdate(id, updates).then((archived, err) => {
				
				if (archived) {
					return `The Product ${id} has been removed`;
				} else {
					return `Failed to remove Product`
				}
			})

			let isLogUpdated = await User.findById(result.id).then(user => {

				let userLog = {

					targetId: id,
					action: `Deactivated a Product`
				
				}

				user.log.push(userLog);
				return user.save().then(user => true).catch(err => err.message)

			})
		}

		//[SUB-SECTION] [ACTIVATE PRODUCT] [RESTORE]

		module.exports.activateProduct = async (id, result, req, res) => {
			
			let updates = {
				isAvailable: true
			}
			
			let productActivate = await Product.findByIdAndUpdate(id, updates).then((archived, err) => {
				
				if (archived) {
					return `The Product ${id} has been restored`;
				} else {
					return `Failed to restore Product`
				}
			})

			let isLogUpdated = await User.findById(result.id).then(user => {

				let userLog = {

					targetId: id,
					action: `Activated a Product`
				
				}

				user.log.push(userLog);
				return user.save().then(user => true).catch(err => err.message)

			})
		}

	//[SUB-SECTION] [ADD STOCK TO PRODUCT] 

		module.exports.addStock = async (id, details, result, userData, req, res) => {
			
			let addedStock = details.stock;
			let oldStock = result.stock;
			let newStack = addedStock + userData.stock

			let updates = {
				stock: newStack,
				isAvailable: true
			}

			let restock = await Product.findByIdAndUpdate(id, updates).then((stockAdded, err) => {
				if (stockAdded) {
					return `${result.name}'s total stock are now ${newStack}`;
				} else {
					return `Failed to add stock`
				}
			})

			let isLogUpdated = await User.findById(result.id).then(user => {

				let userLog = {

					targetId: id,
					action: `Added ${addedStock} stock to ${userData.name} : Now ${newStack}`
				
				}

				user.log.push(userLog);
				return user.save().then(user => true).catch(err => err.message)

			})

		}

//[SECTION] Functionalities [DELETE]