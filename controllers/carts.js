//[SECTION] Dependencies and Modules

	const Cart = require('../models/Cart');

//[SECTION] Functionalities [CREATE]

	module.exports.addToCart = async (data, id) => {

		let productId = data.productId
		let productQuantity = data.quantity
		let name = data.productName
		let price = data.productPrice
		let artist = data.productArtist
		let desc = data.productDescription
		let img = data. productImg
		
		let newProductToCart = new Cart ({
			userId: id,
			productId: productId,
			quantity: productQuantity,
			productName: name,
			productPrice: price,
			productArtist: artist,
			productDescription: desc,
			productImg: img

		});
		let addProduct = await newProductToCart.save().then((addedProduct, err) => {
				if (err) {
					return 'Failed to Add to cart a Product'
				} else {
					return addedProduct
			}	
	 	});
	};

//[SECTION] Functionalities [RETRIEVE]

	//[SUB-SECTION] [GET MY CART]

		module.exports.getMyCart = (id) => {
			
			return Cart.find({isCheckout: false, userId: id}).then(resultOfTheQuery => {
				return resultOfTheQuery;
			});
		};

		module.exports.getSingleMyCart = (id, userId) => {
			
			return Cart.findById(id).then(resultOfTheQuery => {
				return resultOfTheQuery;
			});
		};

//[SECTION] Functionalities [UPDATE]

	module.exports.addProduct = async (id, cartDetails) => {
		console.log(cartDetails)

			let updatedProduct = {
				quantity: cartDetails.quantity + 1
			}
			
			let updateProduct = await Cart.findByIdAndUpdate(id, updatedProduct).then((productUpdated, err) => {
				
				if (err) {
					return 'Failed to update Product';
				} else {
					return 'Successfully Updated Product';
				}
			})
		}

	module.exports.removeProduct = async (id, cartDetails) => {
		console.log(cartDetails)

			if (cartDetails.quantity === 1) {
				return 'quantity must be atleast 1'
			} else {

			let updatedProduct = {
				quantity: cartDetails.quantity - 1
			}
			
			let updateProduct = await Cart.findByIdAndUpdate(id, updatedProduct).then((productUpdated, err) => {
				
				if (err) {
					return 'Failed to update Product';
				} else {
					return 'Successfully Updated Product';
				}
			})
		}
	}

//[SECTION] Functionalities [DELETE]

	module.exports.deleteProduct = async (id) => {

		let deleteProduct = await Cart.findByIdAndRemove(id).then((productDeleted, err) => {
				
				if (err) {
					return 'Failed to remove Product';
				} else {
					return 'Successfully remove Product';
				}
			})

	}