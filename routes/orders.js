//[SECTION] Dependencies and Modules

	const exp = require("express");
	const controller = require('../controllers/orders');
	const controllerUser = require('../controllers/users');
	const controllerProduct = require('../controllers/products');
	const controllerCart = require('../controllers/carts');
	const auth = require("../auth");
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component

	const route = exp.Router();

//[SECTION] [POST] Routes
	
	//[SUB-SECTION] [ADD PRODUCT] Routes

		route.post('/order', verify, (req, res) => {

			if (req.user.isAdmin == true || req.user.isAdminAssistant == true) {
			return res.send ({
				auth: "Unauthorized User",
				message: "Action Forbidden"
			})
		}


			let data = req.body;
			let id = req.user.id;

			controllerUser.getUserData(id).then(userResult => {

			controllerCart.getMyCart(id).then(cartResult => {
				
			controllerProduct.getAllProducts().then(productResult => {

				controller.checkout(id, userResult, cartResult, productResult).then(outcome => {
					res.send(outcome)
				})

			});

			});
				res.send({message: `Success`})
			});

			

		});

//[SECTION] [GET] Routes

	//[SUB-SECTION] [GET-ALL] Route

		route.get('/orders', verify, verifyAdmin, (req, res) => {
			controller.getAllOrders().then(outcome => {
				res.send(outcome);
			});
		});

	//[SUB-SECTION] [GET MY ORDERS]
	
		route.get('/mine', verify, (req, res) => {

			let id = req.user.id;
			
			controller.getMyOrders(id).then(outcome => {
				
				res.send(outcome);
			});
		});

//[SECTION] [PUT] Routes

//[SECTION] [DELETE] Routes

//[SECTION] Export Route System

	module.exports = route;