//[SECTION] Dependencies and Modules

	const exp = require("express");
	const controller = require("../controllers/users");
	const auth = require("../auth");
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component

	const route = exp.Router()

//[SECTION] Routes [POST]

	//[SUB-SECTION] [REGISTER]

		route.post('/register',(req, res) => {

			let userDetails = req.body;
			controller.registerUser(userDetails).then(outcome => {
				res.send(outcome);
			});
		});

	//[SUB-SECTION] [AUTHENTICATION/LOGIN]

		route.post("/login", controller.loginUser);

//[SECTION] Routes [GET]

	//[SUB-SECTION] [GET MY DETAILS]

		route.get("/me", verify, controller.getMyDetails);

	//[SUB-SECTION] [GET ALL ADMIN ASSISTANT]	

		route.get('/assistants', verify, verifyAdmin, (req, res) => {

			if (req.user.isAdmin == false) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}

			controller.getAllAdminAssistant().then(outcome => {
				res.send(outcome)
			});

		});

	//[SUB-SECTION] [GET ALL COSTUMER]	

		route.get('/costumers', verify, verifyAdmin, (req, res) => {

			/*if (req.user.isAdmin == false) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}*/

			controller.getAllCostumer().then(outcome => {
				res.send(outcome)
			});

		});



//[SECTION] Routes [PUT]

	//[SUB-SECTION] [ASSIGN AN ADMIN ASSISTANT]

		route.put('/:id/hire', verify, verifyAdmin, (req, res) => {

			if (req.user.isAdmin == false) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}

			let userId = req.params.id;
			controller.getUserData(req.user.id).then(result => {
			controller.hireAssistant(userId, result).then(resultOfTheFunction => {
				res.send(resultOfTheFunction)
			});
			res.send({message: `You just hired a new Admin Assistant`})	
			})
		});

	//[SUB-SECTION] [REMOVE AN ADMIN ASSISTANT]

		route.put('/:id/fire', verify, verifyAdmin, (req, res) => {

			if (req.user.isAdmin == false) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}

			let userId = req.params.id;
			controller.getUserData(req.user.id).then(result => {
			controller.fireAssistant(userId, result).then(resultOfTheFunction => {
				
			});
			res.send({message: `You fired an Admin Assistant`})	
		
			})
			
		});

	//[SUB-SECTION] [BAN USER]

		route.put('/:id/ban', verify, verifyAdmin, (req, res) => {

			if (req.user.isAdmin == false) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}

			let userId = req.params.id;
			controller.getUserData(req.user.id).then(result => {
			controller.banUser(userId, result).then(resultOfTheFunction => {
			});
			res.send({message: `User will no longer can use this account`})	

			})

		});

	//[SUB-SECTION] [UNBAN USER]

		route.put('/:id/unban', verify, verifyAdmin, (req, res) => {

			if (req.user.isAdmin == false) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}

			let userId = req.params.id;
			controller.getUserData(req.user.id).then(result => {
			controller.unbanUser(userId, result).then(resultOfTheFunction => {
				
			});
			res.send({message: `User can now use this account`})	
			})

		});

	//[SUB-SECTION] [UPDATE MY DETAILS]

		route.put('/update', verify, (req, res) => {

			let id = req.user.id;
			let data = req.body;

			let email = data.email;
			let firstName = data.firstName;
			let lastName = data.lastName;
			let mobileNumber = data.mobileNumber;

			if (email !== '' && firstName !== '' &&  lastName !== '' && mobileNumber !== '') {

				controller.updateMyDetails(id, data).then(outcome => {
				res.send(outcome);
			});

			} else {
				res.send('Incorrect Input, Make sure details are complete')
			}

		});

//[SECTION] Routes [DELETE]

//[SECTION] Export Route System

	module.exports = route;