//[SECTION] Dependencies and Modules
	const exp = require("express");
	const controller = require('../controllers/carts');
	const auth = require("../auth");
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component

	const route = exp.Router();

//[SECTION] [POST] Routes

	//[SUB-SECTION] [ADD TO CART] Routes

		route.post('/add', verify, (req, res) => {

			/*if (req.user.isAdmin == true || req.user.isAdminAssistant == true) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}*/

			let data = req.body;
			let id = req.user.id;

			if (data.quantity < 1) {
				return res.send({message: `quantity cannot be less than 1`})
			} else	{
					controller.addToCart(data, id).then(outcome => {
					
				});

				return res.send({message: `Successfully a new created a Product`})
			}	
			
		});



//[SECTION] [GET] Routes

	//[SUB-SECTION] [GET MY CART]

		route.get('/cart', verify, (req, res) => {

			let id = req.user.id
			
			controller.getMyCart(id).then(outcome => {
				
				res.send(outcome);
			});
		});

//[SECTION] [PUT] Routes

	route.put('/:id/add', verify, (req, res) => {

		if (req.user.isAdmin == true || req.user.isAdminAssistant == true) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}

			let userId = req.user.id
			let id = req.params.id;

				controller.getSingleMyCart(id).then(cartDetails => {
					
				controller.addProduct(id, cartDetails).then(outcome => {
				
					});
				});

				return res.send({message: `Successfully Updated a Product`})

		});


	route.put('/:id/remove', verify, (req, res) => {

		if (req.user.isAdmin == true || req.user.isAdminAssistant == true) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}

			let userId = req.user.id
			let id = req.params.id;

				controller.getSingleMyCart(id).then(cartDetails => {
					
				controller.removeProduct(id, cartDetails, res).then(outcome => {
				
					});
				});

				return res.send({message: `Successfully Updated a Product`})

		});

//[SECTION] [DELETE] Routes

	route.delete('/:id/remove', verify, (req, res) => {

		if (req.user.isAdmin == true || req.user.isAdminAssistant == true) {
				return res.send ({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			}

			let id = req.params.id;

			controller.deleteProduct(id).then(outcome => {
				
				});

				return res.send({message: `Successfully Deleted a Product`})

		});


//[SECTION] Export Route System

	module.exports = route;