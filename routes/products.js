//[SECTION] Dependencies and Modules

	const exp = require("express");
	const controller = require('../controllers/products');
	const controllerUser = require('../controllers/users');
	const auth = require("../auth");
	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component

	const route = exp.Router();

//[SECTION] [POST] Routes
	
	//[SUB-SECTION] [ADD PRODUCT] Routes

		route.post('/create', verify, verifyAdmin, (req, res) => {
			let data = req.body;
			let id = req.user.id;

			controllerUser.getUserData(req.user.id).then(result => {

			controller.createProduct(data, result,id, req, res).then(outcome => {
				res.send(outcome);
			});
				res.send({message: `Successfully a new created a Product`})
			})
		});

//[SECTION] [GET] Routes
	
	//[SUB-SECTION] [GET-ALL] Route

		route.get('/all', (req, res) => {
			controller.getAllProducts().then(outcome => {
				res.send(outcome);
			});
		});

	//[SUB-SECTION] [GET SINGLE PRODUCT]
	
		route.get('/:id', verify, (req, res) => {
			let productId = req.params.id;

			controller.getProduct(productId).then(result => {
				console.log(result)
				return res.send(result);


			})
		})

	//[SUB-SECTION] [GET ALL ACTIVE PRODUCTS]
	
		route.get('/', (req, res) => {
			
			controller.getAllActiveProducts().then(outcome => {
				
				res.send(outcome);
			});
		});

//[SECTION] [PUT] Routes

	//[SUB-SECTION] [UPDATE PRODUCT]

		route.put('/:id', verify, verifyAdmin, (req, res) => {

			let id = req.params.id;
			let details = req.body;

			let productName = details.name;
			let productDescription = details.description;
			let productPrice = details.price;

			if (productName !== '' && productDescription !== '' &&  productPrice !== '') {

			controllerUser.getUserData(req.user.id).then(result => {

				controller.updateProduct(id, details, result, req, res).then(outcome => {
				
				});
				res.send({message: `Successfully Updated a Product`})

			})

			} else {
				res.send('Incorrect Input, Make sure details are complete')
			}

		});

	//[SUB-SECTION] [DEACTIVATE PRODUCT] [SOFT DELETE]

		route.put('/:id/archive', verify, verifyAdmin, (req, res) => {
			
			let productId = req.params.id;

			controllerUser.getUserData(req.user.id).then(result => {
			
			controller.deactivateProduct(productId, result, req, res).then(resultOfTheFunction => {
				
			})

				res.send({message: `Product ${productId} in now not available`})

			})
		});

	//[SUB-SECTION] [ACTIVATE PRODUCT] [RESTORE]

		route.put('/:id/restore', verify, verifyAdmin, (req, res) => {
			
			let productId = req.params.id;

			controllerUser.getUserData(req.user.id).then(result => {
			
			controller.activateProduct(productId, result, req, res).then(resultOfTheFunction => {
				
			})

			res.send({message: `Product ${productId} in now available`})

			})
		});

	//[SUB-SECTION] [ADD STOCK TO PRODUCT] 

		route.put('/:id/stock', verify, verifyAdmin, (req, res) => {
			let productId = req.params.id;
			let details = req.body;
			let addStock = details.stock;

			controller.getProduct(productId).then(result => {
				
				if (addStock >= 1) {

				controllerUser.getUserData(req.user.id).then(userData => {
					
					controller.addStock(productId, details, userData, result, req, res).then(resultOfTheFunction => {
						res.send(resultOfTheFunction);
					})

					

					})
				let newStack = details.stock + result.stock
				res.send(`${result.name}'s total stock are now ${newStack}`);

				} else {
					res.send(`Use positive numbers`)
				}
			})
		});

//[SECTION] [DELETE] Routes

//[SECTION] Export Route System

	module.exports = route;