//[SECTION] Dependencies and Modules

	const jwt = require("jsonwebtoken");
	const dotenv = require("dotenv");

//[SECTION] [CREATE ACCESS TOKEN]

	dotenv.config() 
	const secretCode = process.env.SECRET;

	module.exports.createAccessToken = (user) => {

		const data = {
			id: user._id,
			email: user.email,
			firstName: user.firstName,
			lastName: user.lastName,
			isAdmin: user.isAdmin,
			isAdminAssistant: user.isAdminAssistant,
			isBan: user.isBan,
			isActive: user.isActive
			

		};


		if(user.isBan) {
			return ({
				auth: "Unauthorized User",
				message: "Your account is Banned"
			})
		}

		return jwt.sign(data, secretCode, {});

	};



//[SECTION] [LOGIN VERIFIER]

	module.exports.verify = (req, res, next) => {

		let token = req.headers.authorization;

		if (typeof token === "undefined"){
			return res.send({auth: "Failed", message: "Please Login"});
		} else {
			token = token.slice(7, token.length);

			jwt.verify(token, secretCode, function (err, decodedToken) {
				if (err){
					return res.send({auth: "Failed", message: err.message});
				} else {
					req.user = decodedToken

					next();
				};
			});
		};
	};

//[SECTION] [ADMIN VERIFIER]

	module.exports.verifyAdmin = (req, res, next) => {

		if(req.user.isAdmin) {
			next();
		} else {
			if(req.user.isAdminAssistant) {
				next();
			} else {
				return res.send({
					auth: "Unauthorized User",
					message: "Action Forbidden"
				})
			};
		};
	};