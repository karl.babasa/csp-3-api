//[SECTION] Dependencies and Modules
	
	const mongoose = require("mongoose");

//[SECTION] Schema / Document Blueprint
	
	const orderSchema = new mongoose.Schema({
		userId:{
			type: String,
			required: true
		},

		userFullName:{
			type: String
		},
		cart: [{
			productId: {
				type: String
			},
			productName: {
				type: String
			},
			productQuantity: {
				type: Number
			},
			productPrice: {
				type: Number
			},
			totalPrice: {
				type: Number
			}
		}],
		finalPrice:{
			type: Number,
			default: 0
		},
		addedOn:{
			type: Date,
			default: new Date()
		}
	})

//[SECTION] Model
	
	const Order = mongoose.model("Order", orderSchema);
	module.exports = Order;