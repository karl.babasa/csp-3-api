//[SECTION] Dependencies and Modules
	
	const mongoose = require("mongoose");

//[SECTION] Schema / Document Blueprint
	
	const productSchema = new mongoose.Schema({
		name:{
			type: String,
			required: [true, "Product's Name is Required"]
		},
		artist:{
			type: String,
			required: [true, "Artist is Required"]
		},
		description:{
			type: String,
			required: [true, "Product's Description is Required"]
		},
		price:{
			type: Number,
			required: [true, "Product's Price is Required"]
		},
		img:{
			type: String
		},
		isAvailable:{
			type: Boolean,
			default: false
		},
		stock: {
			type: Number,
			default: 0
		},
		sold: {
			type: Number,
			default: 0
		},
		category: [{
			categoryId: {
				type: String,
			},
			categoryName: {
				type: String,
			}
		}],
		feedback: [{
			userId: {
				type: String
			},
			userName: {
				type: String,
			},
			rating: {
				type: Number,
			},
			comment: {
				type: String
			},
			date: {
				type: String,
				default: `Comment Date Here`
			}
		}],
		createdOn:{
			type: Date,
			default: new Date()
		}
	})

//[SECTION] Model
	
	const Product = mongoose.model("Product", productSchema);
	module.exports = Product;