//[SECTION] Dependencies and Modules
	
	const mongoose = require("mongoose");

//[SECTION] Schema / Document Blueprint
	
	const cartSchema = new mongoose.Schema({
		userId:{
			type: String
			
		},
		productId:{
			type: String,
			required: [true]
		},
		productName:{
			type: String,
			
		},
		productDescription:{
			type: String
			
		},
		productImg:{
			type: String
			
		},
		productArtist:{
			type: String
		},
		productPrice:{
			type: String
		},
		quantity:{
			type: Number,
			required: [true]
		},
		isCheckout: {
			type: Boolean,
			default: false
		},
		addedOn:{
			type: Date,
			default: new Date()
		}
	})

//[SECTION] Model
	
	const Cart = mongoose.model("Cart", cartSchema);
	module.exports = Cart;