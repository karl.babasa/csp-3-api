//[SECTION] Dependencies and Modules
	
	const mongoose = require("mongoose");

//[SECTION] Schema / Document Blueprint
	
	const userSchema = new mongoose.Schema({
		email:{
			type: String,
			required: [true, "Email Address is Required"]
		},
		firstName: {
			type: String,
			required: [true, "First Name is Required"]
		},
		lastName: {
			type: String,
			required: [true, "Last Name is Required"]
		},
		password:{
			type: String,
			required: [true, "Password is Required"]
		},
		mobileNumber: {
			type: String,
			required: [true, "Please Enter Your Mobile Number"]
		},
		isAdmin:{
			type: Boolean,
			default: false
		},
		isAdminAssistant: {
			type: Boolean,
			default: false
		},
		isActive: {
			type: Boolean,
			default: true
		},
		isBan: {
			type: Boolean,
			default: false
		},
		createdOn:{
			type: Date,
			default: new Date()
		},
		cart: [{
			productId: {
				type: String
			},
			productName: {
				type: String
			},
			productCategory: {
				type: String
			},
			ProductQuantity: {
				type: Number
			},
			productPrice: {
				type: Number
			},
			productTotalPrice: {
				type: Number
			}
		}],
		order: [{
			orderId: {
				type: String
			},
			productId: {
				type: String
			},
			productName: {
				type: String
			},
			productQuantity: {
				type: Number
			},
			productTotalPrice: {
			type: Number,
			}
		}],
		
		log: [{
			targetId: {
				type: String
			},
			action: {
				type: String
			},
			actionDate: {
				type: Date,
				default: new Date
			}
		}]
	})

//[SECTION] Model
	
	const User = mongoose.model("User", userSchema);
	module.exports = User;