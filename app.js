//[SECTION] Package and Dependencies

	const express = require("express");
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const productRoutes = require('./routes/products');
	const userRoutes = require('./routes/users')
	const cartRoutes = require('./routes/carts')
	const orderRoutes = require('./routes/orders')
	//const voucherRoutes = require('./routes/vouchers')
	//const categoryRoutes = require('./routes/categories')


//[SECTION] Server Setup

	const app = express();
	dotenv.config() 
	app.use(cors())
	app.use(express.json());
	const secret = process.env.CONNECTION_STRING
	const port = process.env.PORT

//[SECTION] Application Routes

	app.use('/products', productRoutes);
	app.use('/users', userRoutes);
	app.use('/carts', cartRoutes);
	app.use('/orders', orderRoutes);
	//app.use('/vouchers', voucherRoutes);
	//app.use('/categories', categoryRoutes);

//[SECTION] Database Connect

	mongoose.connect(secret)
	let connectStatus = mongoose.connection;
		
	connectStatus.on('open', () => console.log(`Database is Connected`));

//[SECTION] Gateway Responce

	app.get('/', (req, res) => {
		res.send(`E-Commerce Landing Page`)
	})
	app.listen(port, () => console.log(`Server is running on port ${port}`));